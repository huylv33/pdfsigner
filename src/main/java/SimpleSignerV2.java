import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.*;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.PKCSException;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SimpleSignerV2 {
    public static final String SRC = "src/main/resources/Lab07.pdf";
    public static final String DEST = "results/Lab07_signed_V2.pdf";
    public static final boolean withOscp = true;
    public static void main(String[] args)
            throws IOException, GeneralSecurityException, DocumentException {
        String path = "D:/cert/cacert/test.crt";
        char[] pass = "123456".toCharArray();
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        FileReader fileReader1 = new FileReader(path);
        X509Certificate x509Cert = null;

        TSAClient tsaClient = new TSAClientBouncyCastle("http://ts.ssl.com",null, null);

        try (FileReader fileReader = new FileReader(path)) {
            try (PEMParser pemParser = new PEMParser(fileReader)) {
                Object object = null;
                while ((object = pemParser.readObject()) != null) {
                    if (object instanceof X509CertificateHolder) {
                        x509Cert = (X509Certificate) new JcaX509CertificateConverter()
                                .getCertificate((X509CertificateHolder) object);
                        break;
                    }
                }
            }
        }
        Certificate[] chain = new Certificate[]{x509Cert};
        for (int i = 0; i < chain.length; i++) {
            X509Certificate cert = (X509Certificate)chain[i];
            System.out.println(String.format("[%s] %s", i, cert.getSubjectDN()));
            System.out.println(CertificateUtil.getCRLURL(cert));
            System.out.println(CertificateUtil.getOCSPURL(cert));
        }
        OcspClient ocspClient = new OcspClientBouncyCastle(null);
        CrlClient crlClient = new CrlClientOnline(chain);
        List<CrlClient> crlList = new ArrayList<>();
        crlList.add(crlClient);
        PrivateKey pk = null;
        try {
            pk = getPrivateKey("E:/digital-sign/test.key");
        } catch (OperatorCreationException | PKCSException e) {
            e.printStackTrace();
        }
        SimpleSignerV2 app = new SimpleSignerV2();
        app.sign(SRC, DEST, chain, pk, DigestAlgorithms.SHA512,
                provider.getName(),
                MakeSignature.CryptoStandard.CADES,
                "Demo", "Vinh Phuc province", null,
                null, tsaClient, 0);
    }

    public void sign(String src, String dest,
                     Certificate[] chain, PrivateKey pk,
                     String digestAlgorithm, String provider, MakeSignature.CryptoStandard subfilter,
                     String reason, String location,
                     Collection<CrlClient> crlList,
                     OcspClient ocspClient,
                     TSAClient tsaClient,
                     int estimatedSize)
            throws GeneralSecurityException, IOException, DocumentException {
        // Creating the reader and the stamper
        PdfReader reader = new PdfReader(src);
        FileOutputStream os = new FileOutputStream(dest);
        PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
        // Creating the appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        appearance.setReason(reason);
        appearance.setLocation(location);
        appearance.setVisibleSignature(new Rectangle(36, 748, 144, 780), 1, "sig");
        // Creating the signature
        ExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
        ExternalDigest digest = new BouncyCastleDigest();;
        MakeSignature.signDetached(appearance, digest, pks, chain, crlList, ocspClient, tsaClient, estimatedSize, subfilter);
    }

    public static PrivateKey getPrivateKey(String keyFile)
            throws OperatorCreationException, PKCSException, IOException {
        PrivateKey privateKey = null;
        String passphrase = "123456";
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        try (FileReader fileReader = new FileReader(keyFile)) {
            try (PEMParser pemParser = new PEMParser(fileReader)) {
                Object object = null;
                while ((object = pemParser.readObject()) != null) {
                    if (object instanceof PEMEncryptedKeyPair) {
                        if (passphrase == null)
                            throw new IllegalArgumentException("Need passphrase");
                        PEMDecryptorProvider decProv =
                                new JcePEMDecryptorProviderBuilder()
                                        .build(passphrase.toCharArray());
                        var pair = converter.getKeyPair(((PEMEncryptedKeyPair) object)
                                .decryptKeyPair(decProv));
                        privateKey = pair.getPrivate();
                        break;
                    } else if (object instanceof PKCS8EncryptedPrivateKeyInfo) {
                        if (passphrase == null)
                            throw new IllegalArgumentException("Need passphrase");
                        InputDecryptorProvider decryptionProv =
                                new JceOpenSSLPKCS8DecryptorProviderBuilder()
                                        .build(passphrase.toCharArray());
                        privateKey = converter.getPrivateKey(((PKCS8EncryptedPrivateKeyInfo) object)
                                .decryptPrivateKeyInfo(decryptionProv));
                        break;
                    } else if (object instanceof PEMKeyPair) {
                        var pair = converter.getKeyPair((PEMKeyPair) object);
                        privateKey = pair.getPrivate();
                        break;
                    }
                }
            }
        }
        return privateKey;
    }
}
